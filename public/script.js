
function click1() {
    var goods = document.getElementsByName("field1");
    var quantity = document.getElementsByName("field2");
    var r = document.getElementById("result");
    if(check(goods, quantity)) {
        return false;
    }
    r.innerHTML="Стоимость заказа: " +
    (parseInt(goods[0].value)*parseInt(quantity[0].value));
    console.log(r);
    return false;
}

function getVowels(str) {
    var m = str.match(/\D/g);
    if (m === null) {
    return true;
    } else {
        return false;
    }
}

function check(h1, h2) {
    if(parseInt(h1[0].value)<0 || parseInt(h2[0].value)<0){
        alert("Ошибка: числа не должны быть отрицательными");
        return true;
    }
    if(!getVowels(h1[0].value) || !getVowels(h2[0].value)){
        alert("Ошибка: некорректный ввод данных, введите число");
        return true;
    }
    return false;
}

window.addEventListener("DOMContentLoaded", function (event) {
    console.log("DOM fully loaded and parsed");
  });

  document.body.style.background = "#D8BFD8";
